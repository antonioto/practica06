from src.nodos import Nodo

# Definicion de la clase 'ListaLigada':
class ListaLigada:
    def __init__(self):
        self.cabeza = None

    def agregar_elemento(self, valor):
        if self.cabeza == None:
            self.cabeza = Nodo(valor)
        else:
            actual = self.cabeza
            while actual.siguiente != None:
                actual = actual.siguiente
            actual.siguiente = Nodo(valor)
        return

    def buscar_elemento(self, valor):
        if self.cabeza != None:
            actual = self.cabeza
            while actual.siguiente != None:
                if actual.valor == valor:
                    return True
                else:
                    actual = actual.siguiente
        return False

    def elimina_cabeza(self):
        if self.cabeza != None:
            self.cabeza = self.cabeza.siguiente
        return

    def elimina_rabo(self):
        actual = self.cabeza
        if self.cabeza == None:
            return
        elif self.cabeza.siguiente == None:
            self.cabeza = None
        elif actual != None:
            while actual.siguiente.siguiente != None:
                actual = actual.siguiente
            actual.siguiente = None
        return

    def tamagno(self):
        contador = 0
        actual = self.cabeza
        while actual != None:
            contador = contador + 1
            actual = actual.siguiente
        return contador

    def copia(self):
        nueva_lista = ListaLigada()
        actual = self.cabeza
        while actual:
            nueva_lista.agregar_elemento(actual.valor)
            actual = actual.siguiente
        return nueva_lista

    def __str__(self):
        valores = []
        actual = self.cabeza
        while actual:
            valores.append(str(actual.valor))
            actual = actual.siguiente
        return " -> ".join(valores)


 
